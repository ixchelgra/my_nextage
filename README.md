# README #

Brief readme.

### About my_nextage ros package: ###

* Personalized project scenario of nextage using hrpsys simulator.


### Installation Procedure ###

* git clone into your src folder of your ros workspace

**** IMPORTaNT *****

* You need to change the path of the extra objects you add to your project in the file: my_nextage/conf/nextage_table_sim.xml
* Right now it is at line 182: url="[path to your ros workspace]/src/my_nextage/objects/[model_file_name].wrl"
 
 After starting ROS master (roscore), then launch with the command: rtmlaunch my_nextage nextage_ros_bridge_my_simulation.launch
 
 